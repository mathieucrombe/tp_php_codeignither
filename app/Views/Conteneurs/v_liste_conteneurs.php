<article class="main-article">
    <h1>Listes conteneurs (<?php echo $pager->getDetails()['total'] ?> trouvés)</h1>
    <form action="<?php echo base_url('Cconteneur/search'); ?>" method="get">
        <input type="text" name="recherche" placeholder="adresse ou ville" value="<?php if( isset($recherche) == true){ echo $recherche;};?>">
        <input type="submit" value="Rechercher">
    </form>
    <p>
        <?php
        if ($result == null) {
            echo " y'a rien";
        } else {
            foreach ($result as $row) {

                echo $row['Id'] . "-";
                echo $row['AddrEmplacement'] . " ";
        ?>
                <a href="http://conteneurs.local/Cconteneur/detail/<?php echo $row['Id'] ?>">Plus d'info</a>
        <?php
                echo "<br>";
            }
        }
        ?>
        <?php echo $pager->links(); ?>
    </p>
</article>