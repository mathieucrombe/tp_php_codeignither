<?php

namespace App\Models;

use CodeIgniter\Model;

class Mtournee extends Model
{
    protected $table = 'tourneestandard';
    protected $primaryKey = 'Id';
    protected $returnType = 'array';

    public function getAll()
    {
        $requete = $this->select('*') 
                        ->orderBy('Nom', 'ASC');
        return $requete->findAll();
    }
    public function getDetail($prmId)
    {
        $requete = $this->select('*')
            ->where(['Id' => $prmId]);
        return $requete->findAll();
    }
}