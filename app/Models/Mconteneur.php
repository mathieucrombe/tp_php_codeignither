<?php

namespace App\Models;

use CodeIgniter\Model;


class Mconteneur extends Model
{
    protected $table = 'conteneur';
    protected $primaryKey = 'Id';
    protected $returnType = 'array';

    public function getAll()
    {
        $requete = $this->select('Id, AddrEmplacement');
        return $requete->findAll();
    }
    public function getAllWithPager()
    {
        $requete = $this->select('Id, AddrEmplacement');
        return $requete->paginate(10); // 10 lignes par page;
    }
    public function getDetail($prmId)
    {
        $requete = $this->select('AddrEmplacement, LatLng, VolumeMax, VolumeMesureActuel, Nom, JourCollecte')
            ->join('tourneestandard', 'conteneur.TourneeStandardId = tourneestandard.Id', 'left')
            ->where(['conteneur.Id' => $prmId]);
        return $requete->findAll();
    }
    public function getSearchWithPager($prmrecherche)
    {
        $requete = $this->select('Id, AddrEmplacement')->like(['AddrEmplacement' => "$prmrecherche"]);
        return $requete->paginate(10);
    }
    public function getAllByIdTournee($prmId)
    {
        $requete = $this->select('Id, AddrEmplacement')
            ->where(['TourneeStandardId' => $prmId]);
        return $requete->findAll();
    }
    public function createConteneur($prmData)
    {
        //nom des colonnes qui peuvent être modifiées par cette requête 
        //(obligatoire pour les requêtes INSERT et UPDATE)
        $this->allowedFields = ['AddrEmplacement', 'LatLng', 'VolumeMax'];
        $this->insert($prmData);
        //ID du nouvel enregistrement retourné dans un tableau associatif 
        $retour['lastInsertId'] = $this->insertID('Id');
        return $retour;
    }
}
