<?php namespace App\Controllers;
use CodeIgniter\Controller;
class Caccueil extends Controller { 
    public function index() {
        $data['title'] = "Accueil";
        $page['contenu'] = view('v_accueil', $data);
        
        return view('Commun/v_template',$page, $data);
     }
 }

