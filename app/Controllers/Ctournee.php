<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Mtournee;
use App\Models\Mconteneur;
use \CodeIgniter\Exceptions\PageNotFoundException;

class Ctournee extends Controller
{
    public function index()
    {
        $data['title'] = "Liste tournée";
        $model = new Mtournee;
        $data['result'] = $model->getAll();
        $page['contenu'] = view('Tournees/v_liste_tournee', $data);
        return view('Commun/v_template', $page);
    }
    public function detail($prmId = null)
    {
        $data['title'] = "Détail tournée";
        if ($prmId != null) {
            $data['titre3'] = $prmId;
            $model = new Mtournee();
            $data['resultt'] = $model->getDetail($prmId);
            $model = new Mconteneur();
            $data['result'] = $model->getAllByIdTournee($prmId);
            if ($data['result'] != []) {
                $page['contenu'] = view('Tournees/v_detail_tournee', $data);
                return view('Commun/v_template', $page);
            } else {
                throw PageNotFoundException::forPageNotFound("La tournée que vous cherchez n'existe pas");
            }
        } else {
            throw PageNotFoundException::forPageNotFound("La tournée que vous cherchez n'existe pas");

        }
    }
}
