<?php

namespace App\Controllers\Api;

use CodeIgniter\RESTful\ResourceController;

class Conteneurs extends ResourceController
{
    protected $modelName = 'App\Models\Mconteneur';
    protected $model;
    protected $format = 'json';
    public function index()
    {
        $result = $this->model->getAll();
        return $this->respond($result);
    }
    public function show($id = null)
    {
        $result = $this->model->getDetail($id);
        if ($result == Null) {
            return $this->respond($result, 400);
        } else {
            return $this->respond($result);
        }
    }
    public function create()
    {
        $data = $this->request->getPost('dto');
        $data = json_decode($data, true);
        if ($data != null) {
            $result = $this->model->createConteneur($data);
            return $this->respond($result, 201);
        }else{
            return $this->respond($data, 400);
        }
    }
}
