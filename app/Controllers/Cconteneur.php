<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Mconteneur;
use \CodeIgniter\Exceptions\PageNotFoundException;

class Cconteneur extends Controller
{
    public function index()
    {
        $data['title'] = "Les conteneurs de vêtements";

        $model = new Mconteneur();
        $data['result'] = $model->getAllWithPager();
        $data['pager'] = $model->pager;
        $page['contenu'] = view('Conteneurs/v_liste_conteneurs', $data);
        return view('Commun/v_template', $page);
    }
    public function detail($prmId = null)
    {
        $data['title'] = "Détail containeur";
        if ($prmId != null) {
            $data['titre3'] = $prmId;
            $model = new Mconteneur();
            $data['result'] = $model->getDetail($prmId);
            if ($data['result'] != []) {
                $page['contenu'] = view('Conteneurs/v_detail_conteneur', $data);
                return view('Commun/v_template', $page);
            } else {
                throw PageNotFoundException::forPageNotFound("Le containeur que vous cherchez n'éxiste pas");
            }
        } else {
            throw PageNotFoundException::forPageNotFound("Le containeur que vous cherchez n'éxiste pas");
        }
    }
    public function search()
    {
        $searchText = $this->request->getGet('recherche');
        $data['recherche'] = $searchText;
        $data['title'] = "Les conteneurs de vêtements";
        $model = new Mconteneur();
        $data['result'] = $model->getSearchWithPager($searchText);
        $data['pager'] = $model->pager;
        $page['contenu'] = view('Conteneurs/v_liste_conteneurs', $data);
        return view('Commun/v_template', $page);
    }
}
